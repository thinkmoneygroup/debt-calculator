import Vue from 'vue'
import App from './App.vue'
import router from './router'

import EventHub from 'vue-event-hub'
Vue.use(EventHub)

import "./assets/styles.scss"

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
